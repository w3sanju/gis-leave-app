import { Component, NgModule, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titleVar : any;
  constructor(
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ){}
  title = 'xyz';
  ngOnInit() {
    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationEnd) {
    //     // console.log('NavigationEnd:', event.url);
    //     this.router.url == event.url;
    //   }
    //
    // });

    // this.router.events.subscribe((event) => this.titleService.setTitle(event['title']));


    this.router.events
    .filter((event) => event instanceof NavigationEnd)
    .map(() => this.activatedRoute)
    .map((route) => {
      while (route.firstChild) {
        route = route.firstChild;
      }
      return route;
    })
    .filter((route) => route.outlet === 'primary')
    .mergeMap((route) => route.data)
    .subscribe((event) => this.titleService.setTitle(event['title']));
    // console.log(event['title']);

  }
  // title = 'app';
  openNav() {
    document.getElementById('sidenavToggle').style.width = '300px';
    document.getElementById('sidenavToggle').style.transform = 'translateX(0px)';
      // document.getElementById('sidebar').style.width = '300px';
      // document.getElementById('sidebar').style.transform = 'translateX(0px)';

      document.getElementById('sidenav-outer').style.display = 'block';
  }
  closeNav() {
      document.getElementById('sidenavToggle').style.width = '300px';
      document.getElementById('sidenavToggle').style.transform = 'translateX(-300px)';
      document.getElementById('sidenav-outer').style.display = 'none';
  }
  backNav(wer) {
      // this.router.navigate([wer]);
      history.back();
  }
}
