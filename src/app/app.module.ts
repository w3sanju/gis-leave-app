import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import {ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from '../pages/login/login.component';
import { SplashComponent } from '../pages/splash/splash.component';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { SideNavComponent } from '../pages/side-nav/side-nav.component';
import { LeaveManagerComponent } from '../pages/leave-manager/leave-manager.component';
import { ApplyLeaveComponent } from '../pages/apply-leave/apply-leave.component';
import { LeaveDetailComponent } from '../pages/leave-detail/leave-detail.component';
import { LeaveApplicationApprovalComponent } from '../pages/leave-application-approval/leave-application-approval.component';
import { LeaveAppApprovalCategoryComponent } from '../pages/leave-app-approval-category/leave-app-approval-category.component';
import { LeaveAppAppr2Component } from '../pages/leave-app-appr-2/leave-app-appr-2.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent, data: { title: 'Login' } },
  { path: 'splash', component: SplashComponent, data: { title: 'Go to Dashboard' } },
  { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' } },
  { path: 'leave-manager', component: LeaveManagerComponent, data: { title: 'Leave Manager' } },
  { path: 'apply-leave', component: ApplyLeaveComponent, data: { title: 'Leave Manager' } },
  { path: 'leave-detail', component: LeaveDetailComponent, data: { title: 'Leave Manager' } },
  { path: 'leave-application-approval', component: LeaveApplicationApprovalComponent, data: { title: 'Leave Application Approval' } },
  { path: 'leave-app-approval-category', component: LeaveAppApprovalCategoryComponent, data: { title: 'Leave Application Approval' } },
  { path: 'leave-app-appr-2', component: LeaveAppAppr2Component, data: { title: 'Leave Application Approval' } }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SplashComponent,
    DashboardComponent,
    SideNavComponent,
    LeaveManagerComponent,
    ApplyLeaveComponent,
    LeaveDetailComponent,
    LeaveApplicationApprovalComponent,
    LeaveAppApprovalCategoryComponent,
    LeaveAppAppr2Component
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
