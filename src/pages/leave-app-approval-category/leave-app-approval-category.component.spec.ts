import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveAppApprovalCategoryComponent } from './leave-app-approval-category.component';

describe('LeaveAppApprovalCategoryComponent', () => {
  let component: LeaveAppApprovalCategoryComponent;
  let fixture: ComponentFixture<LeaveAppApprovalCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveAppApprovalCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveAppApprovalCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
