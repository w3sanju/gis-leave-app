import { Component, NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  closeNav() {
      document.getElementById('sidenavToggle').style.width = '300px';
      document.getElementById('sidenavToggle').style.transform = 'translateX(300px)';
  }

}
