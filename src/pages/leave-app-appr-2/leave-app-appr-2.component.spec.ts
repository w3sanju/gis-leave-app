import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveAppAppr2Component } from './leave-app-appr-2.component';

describe('LeaveAppAppr2Component', () => {
  let component: LeaveAppAppr2Component;
  let fixture: ComponentFixture<LeaveAppAppr2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveAppAppr2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveAppAppr2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
