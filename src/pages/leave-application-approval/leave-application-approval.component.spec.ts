import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveApplicationApprovalComponent } from './leave-application-approval.component';

describe('LeaveApplicationApprovalComponent', () => {
  let component: LeaveApplicationApprovalComponent;
  let fixture: ComponentFixture<LeaveApplicationApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveApplicationApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveApplicationApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
