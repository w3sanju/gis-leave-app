import 'rxjs/add/operator/switchMap';
import { Component, OnInit, Input, NgModule, Pipe } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
      private router: Router
  ) {}
  myform: FormGroup;
  employeeId: FormControl;
  password: FormControl;

  ngOnInit() {
    // document.title = 'Login';
    this.createFormControls();
    this.createForm();
  }
  createFormControls() {
      this.employeeId = new FormControl('', Validators.required);
      this.password = new FormControl('', Validators.required,);
  }
  createForm() {
      this.myform = new FormGroup({
          employeeId: this.employeeId,
          password: this.password,
      });
  }
  onSubmit() {
      localStorage.clear();
      if (this.myform.valid) {
        // var data = this.myform.value;
        console.log(this.myform.value);
        this.myform.reset();
        // var employeeid = data.employeeId;
        // console.log(employeeid);
        // return this.httpService.post('api/login', data).then(
        //   (success)=> {
        //       console.log(success.data.isSucceeded);
        //       if(success.data.isSucceeded == true){
        //           localStorage.setItem('token', success.data.token);
        //           localStorage.setItem('employeeId', '190771');
        //           this.router.navigate(['/smash']);
        //       }else{
        //           console.log(success.data.message);
        //       }
        //   }).catch(
        //       (err)=> {
        //          console.log(err);
        //          this.router.navigate(['/']);
        //       }
        //    )
        // this.router.navigate(['/splash']);
      }
      this.router.navigate(['/splash']);
  }

}
