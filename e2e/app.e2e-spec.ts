import { GISLeaveAppPage } from './app.po';

describe('gis-leave-app App', () => {
  let page: GISLeaveAppPage;

  beforeEach(() => {
    page = new GISLeaveAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
